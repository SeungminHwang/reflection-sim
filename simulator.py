TIME_UNIT = 1


class world:
	def __init__(self,xsize,ysize,zsize,photons,atoms):
		self.xsize = xsize
		self.ysize = ysize
		self.zsize = zsize
		self photons = photons
		self.atoms = atoms
		self.ticks = 0

	def create_photon(self,new_photon):
		self.photons.append(new_photon)

	def delete_photon(self,photon):
		for i,x in enumerate(self.photons):
			if x.uid == photon.uid:
				self.photons.pop(i)
				return True
		return False

	def create_atom(self,new_atom):
		self.atoms.append(new_atom)

	def inct(self):
		self.sanitize()
		self.time+=TIME_UNIT

	def sanitize(self):
		for photon in self.photons:
			if photon._x>self.xsize or photon._x<0:
				delete_photon(photon)
			if photon._y>self.ysize or photon._y<0:
				delete_photon(photon)
			if photon._z>self.zsize or photon._z<0:
				delete_photon(photon)

class photon:
	def __init__(self, world, loc_init, vel_init):
		self.uid == len(self.world.photons)
